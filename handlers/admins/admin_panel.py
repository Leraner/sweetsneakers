from aiogram.dispatcher import FSMContext

from data import config, database
from data.models import Operator, Admin

from loader import dp, driver
from aiogram import types

from states.admin_panel_state import CreateOperator, CreateAdmin, DeleteAdmin, DeleteOperator

db = database.DBCommands()


@dp.message_handler(text='AllOperators')
async def get_all_operators(message: types.Message):
    if str(message.from_user.id) in config.ADMINS:
        all_operators = await Operator.query.gino.all()
        text = [
            'Все операторы:',
        ]
        for operator in all_operators:
            text.append(operator.username)

        await message.answer('\n'.join(text))


@dp.message_handler(text='AllAdmins')
async def get_all_operators(message: types.Message):
    if str(message.from_user.id) in config.ADMINS:
        all_admins = await Admin.query.gino.all()
        text = [
            'Все администраторы:',
        ]
        for admin in all_admins:
            text.append(admin.username)

        await message.answer('\n'.join(text))


@dp.message_handler(text='DelOperator', state=None)
async def delete_operator(message: types.Message):
    if str(message.from_user.id) in config.ADMINS:
        await message.answer('Выберите оператора которого хотите удалить')
        await delete_operatro_buttons(message=message)
        await DeleteOperator.Q1.set()


async def delete_operatro_buttons(message: types.Message):
    keyboard = types.InlineKeyboardMarkup()
    all_operators = await Operator.query.gino.all()

    for operator in all_operators:
        btn = types.InlineKeyboardButton(text=operator.username,
                                         callback_data=f'del {operator.username}')
        keyboard.add(btn)

    await dp.bot.send_message(message.chat.id, "Все операторы:", reply_markup=keyboard)


@dp.callback_query_handler(text_contains='del', state=DeleteOperator.Q1)
async def get_username_and_delete_operator(call: types.CallbackQuery, state: FSMContext):
    username = call.data.rsplit(' ')[-1]

    user = await db.create_user(username=username)
    await db.create_room(chat_id=user.chat_id)
    await dp.bot.send_message(call.from_user.id, 'Оператора удалён')
    await state.finish()


@dp.message_handler(text='CreateOperator', state=None)
async def create_operator(message: types.Message):
    if str(message.from_user.id) in config.ADMINS:
        await message.answer('Введите username пользовтаеля/администратора')
        await CreateOperator.Q1.set()


@dp.message_handler(state=CreateOperator.Q1)
async def get_operator_username_and_create(message: types.Message, state: FSMContext):
    username = message.text

    if '@' in username:
        username = username[1::]

    operator = await db.create_new_operator(username=username)
    if operator:
        await message.answer('Оператор создан')
        await dp.bot.send_message(operator.operator_id, 'Вас назначили оператором')
    else:
        await message.answer('Ошибка при создании оператора')
    await state.finish()


@dp.message_handler(text='CreateAdmin', state=None)
async def create_admin(message: types.Message):
    if str(message.from_user.id) == config.ADMINS[0]:
        await message.answer('Введите username пользователя/оператора')
        await CreateAdmin.Q1.set()


@dp.message_handler(state=CreateAdmin.Q1)
async def get_username_and_create_admin(message: types.Message, state: FSMContext):
    username = message.text

    if '@' in username:
        username = username[1::]

    await db.create_new_admin(username=username)
    await message.answer('Администратор создан')
    await state.finish()


@dp.message_handler(text='DelAdmin', state=None)
async def delete_admin(message: types.Message):
    if str(message.from_user.id) == config.ADMINS[0]:
        await message.answer('Выберите администратора которого хотите удалить')
        await delete_admin_buttons(message=message)
        await DeleteAdmin.Q1.set()


async def delete_admin_buttons(message):
    keyboard = types.InlineKeyboardMarkup()
    all_admins = await Admin.query.gino.all()

    for admin in all_admins:
        btn = types.InlineKeyboardButton(text=admin.username,
                                         callback_data=f'dela {admin.username}')
        keyboard.add(btn)

    await dp.bot.send_message(message.chat.id, "Все администраторы:", reply_markup=keyboard)


@dp.callback_query_handler(text_contains='dela', state=DeleteAdmin.Q1)
async def get_username_and_delete_admin(call: types.CallbackQuery, state: FSMContext):
    username = call.data.rsplit(' ')[-1]

    user = await db.create_user(username=username, permission=True)
    await db.create_room(chat_id=user.chat_id)
    await dp.bot.send_message(call.from_user.id, 'Администратор удалён')
    await state.finish()
