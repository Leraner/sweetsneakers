from aiogram import types

from data import database, config
from data.config import ADMINS
from data.models import Room
from loader import dp
from .services import check_message
from handlers.users.start import stack

db = database.DBCommands()


@dp.message_handler(content_types=['text', 'photo', 'voice', 'video', 'audio', 'sticker'])
async def bot_echo(message: types.Message):
    """Broadcast function"""

    if str(message.from_user.id) in config.ADMINS:
        return None

    if stack and stack.get('operator_id'):
        operator = await db.get_operator(operator_id=message.from_user.id)
        if not operator:

            current_room = await db.get_room(message.from_user.id)
            room_operator = await db.get_operator(current_room.operator_id)

            if room_operator and room_operator.active_room == current_room.id:
                stack.update({'user_id': current_room.user_chat_id,
                              'operator_id': current_room.operator_id})

                await check_message(message=message, stack=stack, is_operator_active=True)

            elif room_operator and room_operator.active_room != current_room.id:
                await check_message(message=message, stack=stack, is_operator_active=False)
                await dp.bot.send_message(room_operator.operator_id,
                                          f'Вам пришло новое сообщение из комнаты №{current_room.id}')
            else:
                await check_message(message=message, stack=stack, is_operator_active=False)

        else:

            if not operator.active_room:
                return None

            current_room = await Room.query.where(
                Room.id == operator.active_room).gino.first()

            if current_room:
                stack.update({'user_id': current_room.user_chat_id,
                              'operator_id': current_room.operator_id})

                await check_message(message=message, stack=stack, is_operator_active=True)
            else:
                await message.answer('Вы не вошли в комнату')

    else:
        user = await db.get_user(chat_id=types.User.get_current().id)

        if user:
            room = await Room.query.where(Room.user_chat_id == user.chat_id).gino.first()

            await check_message(message=message, stack=stack, is_operator_active=False)

            if room.operator_id:
                await dp.bot.send_message(stack['operator_id'],
                                          f'Вам пришло новое сообщение из комнаты №{room.id}')
        # Updating stack after bot restarting
        else:
            operator = await db.get_operator(operator_id=types.User.get_current().id)
            room = await Room.query.where(Room.id == operator.active_room).gino.first()
            if room:
                stack.update({'user_id': room.user_chat_id,
                              'operator_id': room.operator_id})
                await check_message(message=message, stack=stack, is_operator_active=True)
