from aiogram import types
from aiogram.dispatcher.filters.builtin import CommandStart

from data import database
from data.config import stack, ADMINS, operators
from data.models import Room
# from handlers.admins.admin_panel import operators, notifications
from loader import dp
from utils.misc import rate_limit
# from utils.notify_operators import room_creation

db = database.DBCommands()


@rate_limit(5, 'start')
@dp.message_handler(CommandStart())
async def bot_start(message: types.Message):
    if str(message.from_user.id) in (operators or ADMINS):
        return None

    if not message.from_user.username:
        await message.answer('У вас нет username, введите его в настройках телеграм')
        return None

    if not await db.get_user(message.from_user.id):
        await db.create_user(message.from_user.id, messenger='Telegram')

        room = await db.get_room(message.from_user.id)

        if not room:
            room = await db.create_room(message.from_user.id)

        stack.update({'user_id': room.user_chat_id,
                      'operator_id': room.operator_id})

    await message.answer(f'Здравствуйте, {message.from_user.full_name}!')
