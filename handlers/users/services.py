from data import database
from loader import dp, driver

db = database.DBCommands()


async def check_message(message, stack, is_operator_active) -> None:
    user = await db.get_user(stack['user_id'])
    if is_operator_active:
        if user.messenger == 'Instagram':
            if message.photo:
                await send_instagram_photo_message(message=message, stack=stack)
            elif message.text:
                await send_instagram_text_message(message=message, stack=stack)
        elif user.messenger == 'WhatsApp':
            if message.photo:
                await send_whatsapp_photo_message(message=message, stack=stack)
            elif message.video:
                await send_whatsapp_video_message(message=message, stack=stack)
            elif message.text:
                await send_whatsapp_text_message(message=message, stack=stack)
        if user.messenger == 'Telegram':
            if message.photo:
                await send_photo_message(message=message, stack=stack)
            elif message.voice:
                await send_voice_message(message=message, stack=stack)
            elif message.video:
                await send_video_message(message=message, stack=stack)
            elif message.audio:
                await send_audio_message(message=message, stack=stack)
            elif message.sticker:
                await send_sticker_message(message=message, stack=stack)
            else:
                await send_text_message(message=message, stack=stack)
    else:
        if message.photo:
            await write_db_msgs(text=f'{message.photo[0].file_id} photo', stack=stack)
        elif message.voice:
            await write_db_msgs(text=f'{message.voice.file_id} voice', stack=stack)
        elif message.video:
            await write_db_msgs(text=f'{message.video.file_id} video', stack=stack)
        elif message.sticker:
            await write_db_msgs(text=f'{message.sticker.file_id} sticker', stack=stack)
        elif message.audio:
            await write_db_msgs(text=f'{message.audio.file_id} audio', stack=stack)
        else:
            await write_db_msgs(text=message.text, stack=stack)


async def write_db_msgs(stack, text) -> None:
    await db.create_unread_msg(text=text, sender=stack['user_id'])
    await db.create_msg(text=text, sender=stack['user_id'], recipient=stack['operator_id'],
                        chat_id=stack['user_id'])


async def send_whatsapp_video_message(message, stack) -> None:
    file_name = message.video['file_id']
    await message.video.download(f"media/telegram/videos/{file_name}.mp4")
    if message.caption:
        caption = message.caption
    else:
        caption = ''
    driver.send_media(path=f'media/telegram/videos/{file_name}.mp4', chatid=stack['user_id'], caption=caption)
    await db.create_msg(text=f'{file_name}.mp4 video whatsapp', sender=stack['operator_id'], recipient=stack['user_id'],
                        chat_id=stack['user_id'])


async def send_whatsapp_photo_message(message, stack) -> None:
    file_name = message.photo[-1]['file_id']
    await message.photo[-1].download(f"media/telegram/photos/{file_name}.jpg")
    if message.caption:
        caption = message.caption
    else:
        caption = ''
    driver.send_media(path=f'media/telegram/photos/{file_name}.jpg', chatid=stack['user_id'], caption=caption)
    await db.create_msg(text=f'{file_name}.jpg photo whatsapp', sender=stack['operator_id'], recipient=stack['user_id'],
                        chat_id=stack['user_id'])


async def send_whatsapp_text_message(message, stack) -> None:
    me = await dp.bot.get_me()

    if me.username in message.text:
        message.text = message.text.replace(f'@{me.username}', '')

    driver.send_message_to_id(recipient=stack['user_id'], message=message.text)
    await db.create_msg(text=message.text, sender=stack['operator_id'], recipient=stack['user_id'],
                        chat_id=stack['user_id'])


async def send_instagram_photo_message(message, stack) -> None:
    file_name = message.photo[-1]['file_id']
    await message.photo[-1].download(f"media/telegram/photos/{file_name}.jpeg")
    instagram.direct_send_photo(user_ids=[int(stack['user_id'])], filepath=f'media/telegram/photos/{file_name}.jpeg')
    await db.create_msg(text=f'{file_name} photo instagram', sender=stack['operator_id'], recipient=stack['user_id'],
                        chat_id=stack['user_id'])


async def send_instagram_text_message(message, stack) -> None:
    me = await dp.bot.get_me()

    if me.username in message.text:
        message.text = message.text.replace(f'@{me.username}', '')

    instagram.direct_send(text=message.text, user_ids=[int(stack['user_id'])])
    await db.create_msg(text=message.text, sender=stack['operator_id'], recipient=stack['user_id'],
                        chat_id=stack['user_id'])


async def send_sticker_message(message, stack) -> None:
    if message.from_user.id == stack['operator_id']:
        await db.create_msg(text=message.sticker.file_id, sender=stack['operator_id'], recipient=stack['user_id'],
                            chat_id=stack['user_id'])
        await dp.bot.send_sticker(stack['user_id'], message.sticker.file_id)
    else:
        await db.create_msg(text=message.sticker.file_id, sender=stack['user_id'], recipient=stack['operator_id'],
                            chat_id=stack['user_id'])
        await dp.bot.send_sticker(stack['operator_id'], message.sticker.file_id)


async def send_audio_message(message, stack) -> None:
    if message.from_user.id == stack['operator_id']:
        await db.create_msg(text=message.audio.file_id, sender=stack['operator_id'], recipient=stack['user_id'],
                            chat_id=stack['user_id'])
        await dp.bot.send_audio(stack['user_id'], message.audio.file_id)
    else:
        await db.create_msg(text=message.audio.file_id, sender=stack['user_id'], recipient=stack['operator_id'],
                            chat_id=stack['user_id'])
        await dp.bot.send_audio(stack['operator_id'], message.audio.file_id)


async def send_video_message(message, stack) -> None:
    if message.from_user.id == stack['operator_id']:
        await db.create_msg(text=message.video.file_id, sender=stack['operator_id'], recipient=stack['user_id'],
                            chat_id=stack['user_id'])
        await dp.bot.send_video(stack['user_id'], message.video.file_id)
    else:
        await db.create_msg(text=message.video.file_id, sender=stack['user_id'], recipient=stack['operator_id'],
                            chat_id=stack['user_id'])
        await dp.bot.send_video(stack['operator_id'], message.video.file_id)


async def send_voice_message(message, stack) -> None:
    if message.from_user.id == stack['operator_id']:
        await db.create_msg(text=message.voice.file_id, sender=stack['operator_id'], recipient=stack['user_id'],
                            chat_id=stack['user_id'])
        await dp.bot.send_voice(stack['user_id'], message.voice.file_id)
    else:
        await db.create_msg(text=message.voice.file_id, sender=stack['user_id'], recipient=stack['operator_id'],
                            chat_id=stack['user_id'])
        await dp.bot.send_voice(stack['operator_id'], message.voice.file_id)


async def send_photo_message(message, stack) -> None:
    if message.from_user.id == stack['operator_id']:
        await db.create_msg(text=message.photo[0].file_id, sender=stack['operator_id'], recipient=stack['user_id'],
                            chat_id=stack['user_id'])
        await dp.bot.send_photo(stack['user_id'], message.photo[0].file_id)
    else:
        await db.create_msg(text=message.photo[0].file_id, sender=stack['user_id'], recipient=stack['operator_id'],
                            chat_id=stack['user_id'])
        await dp.bot.send_photo(stack['operator_id'], message.photo[0].file_id)


async def send_text_message(message, stack) -> None:
    me = await dp.bot.get_me()

    if me.username in message.text:
        message.text = message.text.replace(f'@{me.username}', '')

    if message.from_user.id == stack['operator_id']:
        await db.create_msg(text=message.text, sender=stack['operator_id'], recipient=stack['user_id'],
                            chat_id=stack['user_id'])
        await dp.bot.send_message(stack['user_id'], text=message.text)
    else:
        await db.create_msg(text=message.text, sender=stack['user_id'], recipient=stack['operator_id'],
                            chat_id=stack['user_id'])
        await dp.bot.send_message(stack['operator_id'], text=message.text)
