import os

from aiogram import types
from aiogram.dispatcher import FSMContext

from data import database, config
from data.config import del_list_ans
from data.models import Room, Operator, Message, UnReadMessage, Answer
from data.config import stack, operators
from keyboards.default import admins_keyboard
from keyboards.default.admins_keyboard import super_admins_keyboard
from keyboards.default.operators_keyboard import operators_keyboard
from loader import dp, driver
from states.reduction import Reduction, DeleteReduction, EditReduction
from utils.misc import rate_limit

db = database.DBCommands()


@dp.message_handler(commands=['keyboard'])
async def keyboard(message: types.Message):
    try:
        if str(message.from_user.id) in operators:
            await message.answer('Вы открыли клавиатуру', reply_markup=operators_keyboard)
        elif str(message.from_user.id) == config.ADMINS[0]:
            await message.answer('Вы открыли клавиатуру', reply_markup=super_admins_keyboard)
        elif str(message.from_user.id) in config.ADMINS:
            await message.answer('Вы открыли клавиатуру', reply_markup=admins_keyboard)
    except:
        await message.answer('Ошибка при открытии клавитуры')


@rate_limit(5, 'Rooms')
@dp.message_handler(text='Rooms')
async def get_room_list(message: types.Message):
    """Get and output rooms"""
    try:
        if str(message.from_user.id) in operators:
            keyboard = types.InlineKeyboardMarkup()
            rooms = await Room.query.where(Room.active == True).gino.all()
            for room in rooms:
                unread_msgs = len(await UnReadMessage.query.where(UnReadMessage.room_id == room.id).gino.all())

                if room.busy:
                    room_operator = await db.get_operator(room.operator_id)
                    btn = types.InlineKeyboardButton(
                        text=f'Room busy №{room.id} by {room_operator.username} ({unread_msgs})',
                        callback_data=f'room {room.id}')
                else:
                    btn = types.InlineKeyboardButton(text=f'Room №{room.id} ({unread_msgs})',
                                                     callback_data=f'room {room.id}')

                keyboard.add(btn)

            await dp.bot.send_message(message.chat.id, "Rooms:", reply_markup=keyboard)
    except:
        await message.answer('Ошибка при получении списка комнат')


@dp.callback_query_handler(text_contains='room')
async def enter_the_room(call: types.CallbackQuery):
    try:
        if str(call.from_user.id) in operators:
            room_id = int(call.data.rsplit(' ')[1])
            room = await Room.query.where(Room.id == room_id).gino.first()
            if room:
                if room.busy and room.operator_id != call.from_user.id:
                    """Room is busy while operator enters it"""
                    await call.answer('Эта комната занята')
                elif room.busy and room.operator_id == call.from_user.id:
                    """Operator switched room, which he was working"""
                    stack.update({'user_id': room.user_chat_id})
                    await dp.bot.send_message(call.from_user.id, f'Вы переключились в комнату №{room_id}')
                    await Operator.update.values(active_room=room.id).where(
                        Operator.operator_id == call.from_user.id).gino.first()
                    all_unread_msgs = await UnReadMessage.query.where(
                        UnReadMessage.room_id == room.id).gino.all()
                    await send_msgs_from_db(unread=True, all_msgs=all_unread_msgs, call=call)
                    if '@' in room.user_chat_id:
                        driver.chat_send_seen(room.user_chat_id)
                    await UnReadMessage.delete.where(UnReadMessage.room_id == room.id).gino.all()
                if not room.busy and room.operator_id != call.from_user.id:
                    """
                    Operator enter in room, which another operator left.
                    Or operator enter in just created room.
                    """
                    await Room.update.values(busy=True, operator_id=call.from_user.id).where(
                        Room.id == room_id).gino.first()
                    await Operator.update.values(active_room=room.id).where(
                        Operator.operator_id == call.from_user.id).gino.first()
                    stack.update({'user_id': room.user_chat_id,
                                  'operator_id': types.User.get_current().id})
                    await dp.bot.send_message(call.from_user.id, f'Вы вошли в комнату №{room.id}')
                    await UnReadMessage.delete.where(UnReadMessage.room_id == room.id).gino.all()
                    all_msgs = await Message.query.where(Message.room_id == room_id).gino.all()
                    await send_msgs_from_db(unread=False, all_msgs=all_msgs, call=call)
                    if '@' in room.user_chat_id:
                        driver.chat_send_seen(room.user_chat_id)
            else:
                await dp.bot.send_message(call.from_user.id, 'Такой комнаты не существует')
    except ValueError:
        await dp.bot.send_message(call.from_user.id, 'Такой комнаты не существует')
    except:
        await dp.bot.send_message(call.from_user.id, 'Ошибка при входе в комнату')


async def send_msgs_from_db(unread, all_msgs, call):
    """Get unread messages from db and send them"""
    try:
        for msg in all_msgs:
            if not (msg.message_from in operators) or unread:
                if 'voice' in msg.message_text:
                    file = msg.message_text.rsplit(' ')[0]
                    await dp.bot.send_message(call.from_user.id, 'Пользователь:')
                    await dp.bot.send_voice(call.from_user.id, file)
                elif 'video' in msg.message_text:
                    if 'whatsapp' in msg.message_text:
                        file = msg.message_text.split(' ')[0]
                        await dp.bot.send_video(call.from_user.id, open(f'media/whatsapp/videos/{file}', 'rb'))
                    else:
                        file = msg.message_text.rsplit(' ')[0]
                        await dp.bot.send_message(call.from_user.id, 'Пользователь:')
                        await dp.bot.send_video(call.from_user.id, file)
                elif 'audio' in msg.message_text:
                    file = msg.message_text.rsplit(' ')[0]
                    await dp.bot.send_message(call.from_user.id, 'Пользователь:')
                    await dp.bot.send_audio(call.from_user.id, file)
                elif 'sticker' in msg.message_text:
                    file = msg.message_text.rsplit(' ')[0]
                    await dp.bot.send_message(call.from_user.id, 'Пользователь:')
                    await dp.bot.send_sticker(call.from_user.id, file)
                elif 'photo' in msg.message_text:
                    await dp.bot.send_message(call.from_user.id, 'Пользователь:')
                    if 'whatsapp' in msg.message_text:
                        file = msg.message_text.split(' ')[0]
                        await dp.bot.send_photo(call.from_user.id, open(f'media/whatsapp/photos/{file}', 'rb'))
                    elif 'instagram' in msg.message_text:
                        file = msg.message_text.rsplit(' ')[0]
                        await dp.bot.send_photo(call.from_user.id, open(f'media/instagram/photos/{file}', 'rb'))
                    else:
                        file = msg.message_text.rsplit(' ')[0]
                        await dp.bot.send_photo(call.from_user.id, file)
                else:
                    await dp.bot.send_message(call.from_user.id, f'Пользователь: {msg.message_text}')
            else:
                if 'voice' in msg.message_text:
                    file = msg.message_text.rsplit(' ')[0]
                    await dp.bot.send_message(call.from_user.id, 'Оператор:')
                    await dp.bot.send_voice(call.from_user.id, file)
                elif 'video' in msg.message_text:
                    if 'whatsapp' in msg.message_text:
                        file = msg.message_text.split(' ')[0]
                        await dp.bot.send_video(call.from_user.id, open(f'media/telegram/videos/{file}', 'rb'))
                    else:
                        file = msg.message_text.rsplit(' ')[0]
                        await dp.bot.send_message(call.from_user.id, 'Оператор:')
                        await dp.bot.send_video(call.from_user.id, file)
                elif 'audio' in msg.message_text:
                    file = msg.message_text.rsplit(' ')[0]
                    await dp.bot.send_message(call.from_user.id, 'Оператор:')
                    await dp.bot.send_audio(call.from_user.id, file)
                elif 'sticker' in msg.message_text:
                    file = msg.message_text.rsplit(' ')[0]
                    await dp.bot.send_message(call.from_user.id, 'Оператор:')
                    await dp.bot.send_sticker(call.from_user.id, file)
                elif 'photo' in msg.message_text:
                    await dp.bot.send_message(call.from_user.id, 'Оператор:')
                    if 'whatsapp' in msg.message_text:
                        file = msg.message_text.split(' ')[0]
                        await dp.bot.send_photo(call.from_user.id, open(f'media/telegram/photos/{file}', 'rb'))
                    else:
                        file = msg.message_text.rsplit(' ')[0]
                        await dp.bot.send_photo(call.from_user.id, file)
                else:
                    await dp.bot.send_message(call.from_user.id, f'Оператор: {msg.message_text}')
    except:
        await dp.bot.send_message(call.from_user.id, 'Ошибка при выводе истории сообщений')


@rate_limit(5, 'Exit')
@dp.message_handler(text='Exit')
async def get_room_list(message: types.Message):
    """Get and output rooms"""
    try:
        if str(message.from_user.id) in operators:
            keyboard = types.InlineKeyboardMarkup()
            rooms = await Room.query.where(Room.active == True).gino.all()
            for room in rooms:
                if room.operator_id == message.from_user.id:
                    unread_msgs = len(await UnReadMessage.query.where(UnReadMessage.room_id == room.id).gino.all())

                    room_operator = await db.get_operator(room.operator_id)
                    btn = types.InlineKeyboardButton(
                        text=f'Room busy №{room.id} by {room_operator.username} ({unread_msgs})',
                        callback_data=f'exit {room.id}')

                    keyboard.add(btn)

            await dp.bot.send_message(message.chat.id, "Rooms:", reply_markup=keyboard)
    except:
        await message.answer(message.from_user.id, 'Ошибка при сборе комнат на выход')


@dp.callback_query_handler(text_contains='exit')
async def exit_the_room(call: types.CallbackQuery):
    try:
        if str(call.from_user.id) in operators:
            room_id = int(call.data.rsplit(' ')[1])
            room = await Room.query.where(Room.id == room_id).gino.first()
            if room:
                if room.operator_id == call.from_user.id:
                    await room.update(busy=False, operator_id=None).apply()
                    await Operator.update.values(active_room=None) \
                        .where(Operator.operator_id == call.from_user.id).gino.first()
                    stack.update({'operator_id': None, 'user_id': None})
                    await dp.bot.send_message(call.from_user.id, f'Вы вышли из комнаты №{room_id}')
                else:
                    await dp.bot.send_message(call.from_user.id, 'Вы не оператор в этой комнате')
            else:
                await dp.bot.send_message(call.from_user.id, 'Такой комнаты не существует')
    except ValueError:
        await dp.bot.send_message(call.from_user.id, 'Такой комнаты не существует')
    except:
        await dp.bot.send_message(call.from_user.id, 'Ошибка при выходе из комнаты')


@dp.message_handler(text='Info')
async def get_info_about_chat_user(message: types.Message):
    try:
        if str(message.from_user.id) in operators:
            operator = await db.get_operator(operator_id=message.from_user.id)
            if operator.active_room:
                room = await Room.query.where(Room.id == operator.active_room).gino.first()
                user = await db.get_user(chat_id=room.user_chat_id)

                if user.first_name == user.last_name:
                    full_name = f'Имя Фамилия: {user.first_name}'
                else:
                    full_name = f'Имя Фамилия: {user.first_name} {user.last_name}'

                if user.messenger == 'WhatsApp':
                    phone = user.chat_id.split('@')[0]
                else:
                    phone = None

                text = [
                    'Информация о собеседнике:',
                    f'Мессенджер: {user.messenger}',
                    f'Телефон: {phone}',
                    full_name,
                    f'Username: {user.username}',
                ]
                await message.answer('\n'.join(text))
    except:
        await message.answer('Ошибка при получении инфомарции о пользователе')


# Close Room

@dp.message_handler(text='Close')
async def close_room(message: types.Message):
    try:
        if str(message.from_user.id) in operators:
            operator = await db.get_operator(operator_id=message.from_user.id)
            room = await Room.query.where(Room.id == operator.active_room).gino.first()
            await room.update(active=False, operator_id=None, busy=False).apply()
            await operator.update(active_room=None).apply()
            stack.update({'operator_id': None, 'user_id': None})
            await message.answer('Комната закрыта')
    except:
        await message.answer('Ошибка при закрытии команты')


# AutoAnswers

@dp.message_handler(text='AllAnswers')
async def all_ans(message: types.Message):
    try:
        keyboard = types.InlineKeyboardMarkup()
        reductions = await db.get_answers()
        for reduction in reductions:
            btn = types.InlineKeyboardButton(text=reduction.reduction,
                                             switch_inline_query_current_chat=reduction.full_answer)
            keyboard.add(btn)

        await dp.bot.send_message(message.chat.id, "Все автоответы", reply_markup=keyboard)
    except:
        await message.answer('Ошибка при сборе автоответов')


@dp.message_handler(text='DelAnswers')
async def delete_ans(message: types.Message):
    try:
        if str(message.from_user.id) in operators:
            await message.answer('Выберите автоответ(ы) который хотите удалить')
            await delans_buttons(message=message)
            await DeleteReduction.Q1.set()
    except:
        await message.answer('Ошибка при сборек автоответов на удаление')


async def delans_buttons(message: types.Message):
    try:
        keyboard = types.InlineKeyboardMarkup()
        all_ans = await Answer.query.where(Answer.operator_id == message.from_user.id).gino.all()

        for ans in all_ans:
            btn = types.InlineKeyboardButton(text=ans.reduction,
                                             callback_data=f'del {ans.id}')
            keyboard.add(btn)

        btn1 = types.InlineKeyboardButton(text='Окей',
                                          callback_data='del ok')
        btn2 = types.InlineKeyboardButton(text='Отмена',
                                          callback_data='del no')
        keyboard.add(btn1, btn2)

        await dp.bot.send_message(message.chat.id, "Все ваши автоответы:", reply_markup=keyboard)
    except:
        await message.answer('Ошибка при создании кнопок на автоответы')


@dp.callback_query_handler(text_contains='del', state=DeleteReduction.Q1)
async def filter_delete_ans(call: types.CallbackQuery, state: FSMContext):
    try:
        autoans_id = call.data.rsplit(' ')[-1]

        if 'ok' in call.data:
            await state.update_data(del_list_ans=del_list_ans)
            await dp.bot.send_message(call.from_user.id, 'Сохранено')
            await del_ans(state=state, call=call)
        elif 'no' in call.data:
            await state.finish()
            await dp.bot.send_message(call.from_user.id, 'Отменено')
            return None
        else:
            if autoans_id not in del_list_ans:
                del_list_ans.append(int(autoans_id))
    except:
        await dp.bot.send_message(call.from_user.id, 'Ошибка при получении автоответов на удаление')
        await state.finish()


async def del_ans(call: types.CallbackQuery, state: FSMContext):
    try:
        data = await state.get_data()

        del_list_ans = data.get('del_list_ans')

        all_ans = await Answer.query.where(Answer.operator_id == call.from_user.id).gino.all()

        for ans in all_ans:
            if ans.id in del_list_ans:
                await ans.delete()

        del_list_ans.clear()
        await dp.bot.send_message(call.from_user.id, 'Автоответ(ы) удален(ы)')
    except:
        await dp.bot.send_message(call.from_user.id, 'Ошибка при удалении автоответов')

    await state.finish()


@dp.message_handler(text='EditAnswers')
async def delete_ans(message: types.Message):
    try:
        if str(message.from_user.id) in operators:
            await message.answer('Выберите автоответ(ы) который хотите редактировать')
            await editans_buttons(message=message)
            await EditReduction.Q1.set()
    except:
        await message.answer('Ошибка при подборе автоответов на редактирование')


async def editans_buttons(message: types.Message):
    try:
        keyboard = types.InlineKeyboardMarkup()
        all_ans = await Answer.query.where(Answer.operator_id == message.from_user.id).gino.all()

        for ans in all_ans:
            btn = types.InlineKeyboardButton(text=ans.reduction,
                                             callback_data=f'edit {ans.id}')
            keyboard.add(btn)

        await dp.bot.send_message(message.chat.id, "Все ваши автоответы:", reply_markup=keyboard)
    except:
        await message.answer('Ошибка при создании кнопок на автоответы')


@dp.callback_query_handler(text_contains='edit', state=EditReduction.Q1)
async def filter_delete_ans(call: types.CallbackQuery, state: FSMContext):
    try:
        autoans_id = call.data.rsplit(' ')[-1]

        await state.update_data(autoans_id=autoans_id)
        await dp.bot.send_message(call.from_user.id, 'Введите title кнопки')
        await EditReduction.Q2.set()
    except:
        await dp.bot.send_message(call.from_user.id, 'Ошибка при получении автоответов на удаление')


@dp.message_handler(state=EditReduction.Q2)
async def get_title_autoans(message: types.Message, state: FSMContext):
    try:
        title = message.text
        await state.update_data(title=title)
        await message.answer('Введите автоответ')
        await EditReduction.Q3.set()
    except:
        await message.answer('Ошибка при вводе title автоответа')


@dp.message_handler(state=EditReduction.Q3)
async def get_autoans_and_edit(message: types.Message, state: FSMContext):
    try:
        autoans = message.text

        data = await state.get_data()
        title = data.get('title')
        autoans_id = int(data.get('autoans_id'))

        all_ans = await Answer.query.where(Answer.operator_id == message.from_user.id).gino.all()
        for ans in all_ans:
            if ans.id == autoans_id:
                await ans.update(reduction=title, full_answer=autoans).apply()

        await message.answer('Автоответ отредактирован')
    except:
        await message.answer('Ошибка при редактировании автоответа')

    await state.finish()


@dp.message_handler(text='CreateAnswers', state=None)
async def add_reduction(message: types.Message):
    try:
        if str(message.from_user.id) in operators:
            await message.answer('Введите title для кнопки')
            await Reduction.Q1.set()
    except:
        await message.answer('Ошибка при вводе title автоответа')


@dp.message_handler(state=Reduction.Q1)
async def get_title(message: types.Message, state: FSMContext):
    try:
        title = message.text
        await state.update_data(title=title)
        await message.answer('Введите текст')
        await Reduction.Q2.set()
    except:
        await message.answer('Ошибка при вводе текста автоответа')


@dp.message_handler(state=Reduction.Q2)
async def get_and_create_ans(message: types.Message, state: FSMContext):
    try:
        full_answer = message.text
        data = await state.get_data()
        title = data.get('title')
        await db.create_answer(reduction=title, full_answer=full_answer)
        await message.answer('Автоответ создан')
    except:
        await message.answer('Ошибка при создании автоответа')

    await state.finish()
