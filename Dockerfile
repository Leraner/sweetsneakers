FROM python:latest

RUN mkdir /src
WORKDIR /src
COPY requirements.txt /src/
RUN pip install --no-cache-dir --trusted-host pypi.python.org -r requirements.txt
COPY . /src