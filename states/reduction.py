from aiogram.dispatcher.filters.state import StatesGroup, State


class Reduction(StatesGroup):
    Q1 = State()
    Q2 = State()


class DeleteReduction(StatesGroup):
    Q1 = State()

class EditReduction(StatesGroup):
    Q1 = State()
    Q2 = State()
    Q3 = State()
