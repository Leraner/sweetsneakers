from aiogram.dispatcher.filters.state import StatesGroup, State


class CreateOperator(StatesGroup):
    Q1 = State()


class DeleteOperator(StatesGroup):
    Q1 = State()


class CreateAdmin(StatesGroup):
    Q1 = State()


class DeleteAdmin(StatesGroup):
    Q1 = State()
