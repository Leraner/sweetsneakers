import datetime

from data.config import delta, operators
from data.database import DBCommands


class WhatsAppBroadCast(DBCommands):
    @classmethod
    async def create(cls, driver, bot):
        self = WhatsAppBroadCast()
        self.driver = driver
        self.bot = bot

        await self.get_unread_messages()

    async def get_unread_messages(self):
        for contact in self.driver.get_unread(use_unread_count=40):
            for message in contact.messages:
                room, room_operator = await self.get_room_or_create(message=message)

                if not room.active:
                    await room.update(active=True).apply()

                if message.type == 'chat':
                    await self.create_text_message(operator=room_operator, message=message, room=room)
                elif message.type == 'image':
                    await self.create_image_message(operator=room_operator, message=message, room=room)
                elif message.type == 'video':
                    await self.create_video_message(operator=room_operator, message=message, room=room)

            self.driver.chat_send_seen(contact.chat.id)

        await self.refresh()

    async def get_room_or_create(self, message):
        user = await self.get_user(chat_id=message.sender.id)

        if not user:
            full_name = message.sender.push_name

            if 2 <= len(full_name.split(' ')) > 1:
                first_name = full_name.split(' ')[0]
                last_name = full_name.split(' ')[1]

                await self.create_user(chat_id=message.sender.id, messenger='WhatsApp', last_name=last_name,
                                       first_name=first_name)
            else:
                await self.create_user(chat_id=message.sender.id, messenger='WhatsApp', last_name=full_name,
                                       first_name=full_name)

            room = await self.create_room(chat_id=message.sender.id)

            for operator in operators:
                await self.bot.send_message(operator, f'Создана комната №{room.id}')

            return room, None
        else:
            room = await self.get_room(chat_id=message.sender.id)
            room_operator = await self.get_operator(room.operator_id)
            return room, room_operator

    async def create_video_message(self, operator, message, room):
        video = message.save_media('media/whatsapp/videos/', True)
        video_name = video.split('/')[-1]

        await self.create_msg(text=f'{video_name} video whatsapp', sender=message.sender.id,
                              recipient=operator.operator_id, chat_id=message.sender.id)
        if operator or (operator and operator.active_room == room.id):
            await self.bot.send_video(operator.operator_id,
                                      video=open(f'media/whatsapp/videos/{video_name}', 'rb'))
        else:
            if operator:
                await self.bot.send_message(operator.operator_id, f'Вам пришло сообщение из комнаты №{room.id}')
            await self.create_unread_msg(text=f'{video_name} video whatsapp', sender=message.sender.id)

    async def create_image_message(self, operator, message, room):
        img = message.save_media('media/whatsapp/photos/', True)
        photo_name = img.split('/')[-1]
        await self.create_msg(text=f'{photo_name} photo whatsapp', sender=message.sender.id,
                              recipient=operator.operator_id, chat_id=message.sender.id)

        if operator or (operator and operator.active_room == room.id):
            await self.bot.send_photo(operator.operator_id,
                                      photo=open(f'media/whatsapp/photos/{photo_name}', 'rb'))
        else:
            if operator:
                await self.bot.send_message(operator.operator_id, f'Вам пришло сообщение из комнаты №{room.id}')
            await self.create_unread_msg(text=f'{photo_name} photo whatsapp', sender=message.sender.id)

    async def create_text_message(self, operator, message, room):
        await self.create_msg(text=message.content, sender=message.sender.id,
                              chat_id=message.sender.id)

        if operator or (operator and operator.active_room == room.id):
            await self.bot.send_message(operator.operator_id, message.content)
        else:
            if operator:
                await self.bot.send_message(operator.operator_id, f'Вам пришло сообщение из комнаты №{room.id}')
            await self.create_unread_msg(sender=message.sender.id, text=message.content)

    async def refresh(self):
        now = datetime.datetime.now()

        if now >= delta['now']:
            self.driver.refresh_current_page()
            delta.update({'now': (datetime.datetime.now() + datetime.timedelta(seconds=60))})
