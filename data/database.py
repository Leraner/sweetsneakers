import datetime

from aiogram import types
from gino.schema import GinoSchemaVisitor

from . import config
from .config import db_name, db_pass, db_user, host, operators, stack
from .models import db, User, Room, Message, UnReadMessage, Operator, Answer, Admin


class DBCommands:
    async def get_user(self, chat_id) -> User:
        user = await User.query.where(User.chat_id == str(chat_id)).gino.first()
        return user

    async def get_room(self, chat_id) -> Room:
        room = await Room.query.where(Room.user_chat_id == str(chat_id)).gino.first()
        return room

    async def get_operator(self, operator_id) -> Operator:
        operator = await Operator.query.where(Operator.operator_id == operator_id).gino.first()
        return operator

    async def get_answers(self) -> list:
        operator = types.User.get_current()
        answers = await Answer.query.where(Answer.operator_id == operator.id).gino.all()
        return answers

    async def create_answer(self, reduction, full_answer) -> Answer:
        current_operator = types.User.get_current()

        new_answer = Answer()
        new_answer.operator_id = current_operator.id
        new_answer.reduction = reduction
        new_answer.full_answer = full_answer

        await new_answer.create()
        return new_answer

    async def create_user(
            self, chat_id=None, messenger=None, username=None, permission=False, last_name=None, first_name=None
    ) -> User:
        old_user = await User.query.where(User.username == username).gino.first()
        admin = await Admin.query.where(Admin.username == username).gino.first()
        operator = await Operator.query.where(Operator.username == username).gino.first()

        if chat_id:
            old_user = await self.get_user(chat_id)

        data = {}

        if old_user:
            return old_user
        elif admin and permission:
            data.update({
                'chat_id': str(admin.admin_id),
                'username': admin.username,
                'last_name': admin.last_name,
                'first_name': admin.first_name,
                'messenger': 'Telegram',
            })
            config.ADMINS.remove(str(admin.admin_id))
            await admin.delete()
        elif operator:
            data.update({
                'chat_id': str(operator.operator_id),
                'username': operator.username,
                'last_name': operator.last_name,
                'first_name': operator.first_name,
                'messenger': 'Telegram',
            })
            await Room.update.values(operator_id=None).where(Room.operator_id == operator.operator_id).gino.all()
            await Message.delete.where(Message.message_from == str(operator.operator_id)).gino.all()
            await UnReadMessage.delete.where(UnReadMessage.message_from == str(operator.operator_id)).gino.all()
            operators.remove(str(operator.operator_id))
            await operator.delete()
            stack.update({'operator_id': None, 'user_id': None})
        else:
            if messenger == 'Telegram':
                user = types.User.get_current()
                data.update({
                    'chat_id': str(chat_id),
                    'username': user.username,
                    'last_name': user.last_name,
                    'first_name': user.first_name,
                    'messenger': messenger,
                })
            else:
                data.update({
                    'chat_id': str(chat_id),
                    'messenger': messenger,
                    'username': None,
                    'last_name': last_name,
                    'first_name': first_name
                })

        new_user = User()
        new_user.messenger = data.get('messenger')
        new_user.username = data.get('username')
        new_user.last_name = data.get('last_name')
        new_user.first_name = data.get('first_name')
        new_user.chat_id = data.get('chat_id')

        await new_user.create()
        return new_user

    async def create_room(self, chat_id) -> Room:
        room = Room()
        room.operator_id = None
        room.user_chat_id = str(chat_id)

        await room.create()
        return room

    async def create_new_admin(self, username) -> Admin:
        user = await User.query.where(User.username == username).gino.first()
        operator = await Operator.query.where(Operator.username == username).gino.first()
        admin = await Admin.query.where(Admin.username == username).gino.first()
        data = {}

        if user:
            data.update({
                'admin_id': int(user.chat_id),
                'username': user.username,
                'last_name': user.last_name,
                'first_name': user.first_name
            })
            await Room.delete.where(Room.user_chat_id == user.chat_id).gino.first()
            await Message.delete.where(Message.message_from == user.chat_id).gino.all()
            await UnReadMessage.delete.where(UnReadMessage.message_from == user.chat_id).gino.all()
            await user.delete()
        elif operator:
            data.update({
                'admin_id': operator.operator_id,
                'username': operator.username,
                'last_name': operator.last_name,
                'first_name': operator.first_name
            })
            await Room.update.values(operator_id=None).where(Room.operator_id == operator.operator_id).gino.all()
            await Message.delete.where(Message.message_from == str(operator.operator_id)).gino.all()
            await UnReadMessage.delete.where(UnReadMessage.message_from == str(operator.operator_id)).gino.all()
            await operator.delete()
            operators.remove(str(operator.operator_id))
            stack.update({'operator_id': None, 'user_id': None})
        elif admin:
            return admin

        new_admin = Admin()
        new_admin.username = data.get('username')
        new_admin.admin_id = data.get('admin_id')
        new_admin.first_name = data.get('first_name')
        new_admin.last_name = data.get('last_name')

        config.ADMINS.append(str(data.get('admin_id')))
        await new_admin.create()
        return new_admin

    async def create_new_operator(self, username) -> Operator:
        user = await User.query.where(User.username == username).gino.first()
        operator = await Operator.query.where(Operator.username == username).gino.first()
        admin = await Admin.query.where(Admin.username == username).gino.first()
        data = {}

        if user:
            data.update({
                'operator_id': int(user.chat_id),
                'username': user.username,
                'last_name': user.last_name,
                'first_name': user.first_name
            })
            await Room.delete.where(Room.user_chat_id == user.chat_id).gino.first()
            await Message.delete.where(Message.message_from == user.chat_id).gino.all()
            await UnReadMessage.delete.where(UnReadMessage.message_from == user.chat_id).gino.all()
            await user.delete()
        elif operator:
            return operator
        elif admin:
            data.update({
                'operator_id': admin.admin_id,
                'username': admin.username,
                'last_name': admin.last_name,
                'first_name': admin.first_name
            })
            config.ADMINS.remove(str(admin.admin_id))
            await admin.delete()

        new_operator = Operator()
        new_operator.username = data.get('username')
        new_operator.operator_id = data.get('operator_id')
        new_operator.first_name = data.get('first_name')
        new_operator.last_name = data.get('last_name')

        await new_operator.create()
        operators.append(str(data.get('operator_id')))
        return new_operator

    async def create_msg(self, text, sender, chat_id, item_id=None, recipient=None) -> Message:
        room = await Room.query.where(Room.user_chat_id == str(chat_id)).gino.first()

        msg = Message()
        msg.room_id = room.id
        msg.message_text = text
        msg.message_from = str(sender)
        msg.item_id = str(item_id)
        msg.message_to = str(recipient)
        msg.message_time = datetime.datetime.strptime(
            datetime.datetime.now().strftime("%Y/%m/%d %H:%M"),
            "%Y/%m/%d %H:%M")

        await msg.create()
        return msg

    async def create_unread_msg(self, text, sender, item_id=None) -> UnReadMessage:
        room = await Room.query.where(Room.user_chat_id == sender).gino.first()

        msg = UnReadMessage()
        msg.room_id = room.id
        msg.message_text = text
        msg.item_id = str(item_id)
        msg.message_from = str(sender)
        msg.message_time = datetime.datetime.strptime(
            datetime.datetime.now().strftime("%Y/%m/%d %H:%M"),
            "%Y/%m/%d %H:%M")

        await msg.create()
        return msg


async def create_db():
    await db.set_bind(f'postgresql+asyncpg://{db_user}:{db_pass}@{host}/{db_name}')
    db.gino: GinoSchemaVisitor
    # await db.gino.drop_all()
    await db.gino.create_all()


async def load_operators():
    """Loading operators"""
    db_operators = await Operator.query.gino.all()
    for operator in db_operators:
        operators.append(str(operator.operator_id))


async def load_admins():
    db_admins = await Admin.query.gino.all()
    for admin in db_admins:
        config.ADMINS.append(str(admin.admin_id))
