import datetime

from environs import Env

# Теперь используем вместо библиотеки python-dotenv библиотеку environs
env = Env()
env.read_env()

BOT_TOKEN = env.str("BOT_TOKEN")  # Забираем значение типа str
ADMINS = env.list("ADMINS")  # Тут у нас будет список из админов

db_user = env.str("DB_USER")
db_pass = env.str("DB_PASS")
db_name = env.str("DB_NAME")
host = env.str('HOST')

stack = {'user_id': None, 'operator_id': None, 'seen_messages': None}
operators = []
del_list_ans = []

delta = {'now': datetime.datetime.now()}

instagram_username = env.str("INSTAGRAM_USERNAME")
instagram_password = env.str("INSTAGRAM_PASSWORD")
