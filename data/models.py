from gino import Gino
from sqlalchemy import (
    Column, Integer, BigInteger, String, Sequence, ForeignKey, Boolean, Text, TIMESTAMP
)
from sqlalchemy import sql
from sqlalchemy.orm import relationship

db = Gino()


class Admin(db.Model):
    __tablename__ = 'Admin'
    id = Column(Integer, Sequence('admin_id_seq'), unique=True)
    username = Column(String(30))
    admin_id = Column(BigInteger, primary_key=True)
    last_name = Column(String(30), nullable=True)
    first_name = Column(String(30), nullable=True)
    query: sql.Select


class Operator(db.Model):
    """Operator model"""
    __tablename__ = 'Operator'
    id = Column(Integer, Sequence('operator_id_seq'), unique=True)
    username = Column(String(30))
    operator_id = Column(BigInteger, primary_key=True)
    last_name = Column(String(30), nullable=True)
    first_name = Column(String(30), nullable=True)
    active_room = Column(Integer, default=None)
    room = relationship('Room', backref='operator', lazy='dynamic')
    answer = relationship('Answer', backref='operator', lazy='dynamic')
    query: sql.Select


class User(db.Model):
    """User model"""
    __tablename__ = 'User'
    id = Column(Integer, Sequence('user_id_seq'), unique=True)
    chat_id = Column(String, primary_key=True)
    messenger = Column(String)
    last_name = Column(String)
    first_name = Column(String)
    username = Column(String, nullable=True)
    room = relationship('Room', uselist=True, backref='user', lazy='dynamic')
    query: sql.Select


class Room(db.Model):
    """Room model"""
    __tablename__ = 'Room'
    id = Column(Integer, Sequence('room_id_seq'), primary_key=True, unique=True)
    operator_id = Column(BigInteger, ForeignKey('Operator.operator_id'), unique=False)
    user_chat_id = Column(String, ForeignKey('User.chat_id'), unique=True)
    busy = Column(Boolean, default=False)
    messages_id = Column(Integer, ForeignKey('Message.id'), unique=True)
    unread_messages_id = Column(Integer, ForeignKey('UnReadMessage.id'), unique=True)
    seen_messages = Column(String, nullable=True, default=None)
    active = Column(Boolean, default=True)
    query: sql.Select


class Answer(db.Model):
    """Blank answers model"""
    __tablename__ = 'Answers'
    id = Column(Integer, Sequence('ans_id_seq'), primary_key=True, unique=True)
    operator_id = Column(BigInteger, ForeignKey('Operator.operator_id'), unique=False)
    reduction = Column(String(15))
    full_answer = Column(String)
    query: sql.Select


class Message(db.Model):
    """Message model"""
    __tablename__ = 'Message'
    id = Column(Integer, Sequence('messages_id_seq'), primary_key=True, unique=True)
    room = relationship("Room", uselist=True, back_populates="messages")
    room_id = Column(Integer)
    message_text = Column(Text)
    item_id = Column(String, nullable=True)
    message_from = Column(String)
    message_to = Column(String, nullable=True)
    message_time = Column(TIMESTAMP)
    query: sql.Select


class UnReadMessage(db.Model):
    """Unread message model"""
    __tablename__ = 'UnReadMessage'
    id = Column(Integer, Sequence('unread_messages_id_seq'), primary_key=True, unique=True)
    room = relationship("Room", uselist=True, back_populates="unread_messages")
    room_id = Column(Integer)
    message_text = Column(Text)
    item_id = Column(String, nullable=True)
    message_from = Column(String)
    message_time = Column(TIMESTAMP)
    query: sql.Select
