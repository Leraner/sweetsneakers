from aiogram.types import KeyboardButton, ReplyKeyboardMarkup, InlineKeyboardMarkup

admins_keyboard = ReplyKeyboardMarkup(
    keyboard=[
        [
            KeyboardButton(text='AllOperators'),
            KeyboardButton(text='AllAdmins'),
            KeyboardButton(text='CreateOperator'),
            KeyboardButton(text='DelOperator'),
        ],
    ],
    resize_keyboard=True
)

super_admins_keyboard = ReplyKeyboardMarkup(
    keyboard=[
        [
            KeyboardButton(text='AllOperators'),
            KeyboardButton(text='CreateOperator'),
            KeyboardButton(text='DelOperator'),

        ],
        [
            KeyboardButton(text='AllAdmins'),
            KeyboardButton(text='CreateAdmin'),
            KeyboardButton(text='DelAdmin'),
        ]
    ],
    resize_keyboard=True
)