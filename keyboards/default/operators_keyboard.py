from aiogram.types import KeyboardButton, ReplyKeyboardMarkup, InlineKeyboardMarkup

operators_keyboard = ReplyKeyboardMarkup(
    keyboard=[
        [
            KeyboardButton(text='Rooms'),
            KeyboardButton(text='Exit'),
            KeyboardButton(text='Close'),
            KeyboardButton(text='Info')
        ],
        [
            KeyboardButton(text='AllAnswers'),
            KeyboardButton(text='DelAnswers'),
            KeyboardButton(text='CreateAnswers'),
            KeyboardButton(text='EditAnswers')
        ]
    ],
    resize_keyboard=True
)


