import requests

from data.config import operators, stack
from data.database import DBCommands
from data.models import Message, Room


class InstagramBroadCast(DBCommands):

    @classmethod
    async def create(cls, driver, bot):
        self = InstagramBroadCast()
        self.driver = driver
        self.bot = bot

        await self.get_unread_messages()

    async def get_unread_messages(self):
        threads = self.driver.direct_threads()
        for thread in threads:
            user_pk = str(thread.users[0].pk)
            if thread.read_state == 1:
                for message in thread.messages[::-1]:

                    room = await self.get_room_or_create(user_pk=user_pk)
                    messages_id = await self.filter_messages_from_db(room)

                    if (message.id not in messages_id) and (message.item_type == 'text') and (
                            message.user_id == thread.users[0].pk):

                        await self.create_text_message(message=message, room=room)

                    elif message.media and (message.id not in messages_id) and (message.media['media_type'] == 1) and (
                            message.user_id == thread.users[0].pk):

                        await self.create_image_message(message=message, room=room)

                self.driver.direct_send_seen(thread_id=thread.id)

            await self.check_unread(user_pk=user_pk, thread=thread)

    async def get_room_or_create(self, user_pk):
        user = await self.get_user(chat_id=user_pk)
        if not user:
            await self.create_user(chat_id=user_pk, messenger='Instagram')
            room = await self.create_room(chat_id=user_pk)

            for operator in operators:
                await self.bot.send_message(operator, f'Создана новая комната №{room.id}')

            return room
        else:
            room = await self.get_room(chat_id=user_pk)
            return room

    async def check_unread(self, user_pk, thread):
        if stack.get('operator_id') and stack['user_id'] == user_pk:
            if len(thread.last_seen_at) == 2 and (
                    thread.last_seen_at[f'{user_pk}']['item_id'] != stack['seen_messages']):
                await self.bot.send_message(stack['operator_id'], 'Сообщения прочитаны')
                stack.update({'seen_messages': thread.last_seen_at[f'{user_pk}']['item_id']})

            elif stack['seen_messages'] is None and len(thread.last_seen_at) == 2:
                stack.update({'seen_messages': thread.last_seen_at[f'{user_pk}']['item_id']})

    async def create_image_message(self, message, room):
        url = message.media['image_versions2']['candidates'][0]['url']
        photo = requests.get(url)
        photo_name = message.media['id']
        with open(f'media/instagram/photos/{photo_name}.jpg', 'wb') as f:
            f.write(photo.content)
            f.close()

        await self.create_msg(
            text=f'{photo_name}.jpg photo instagram', sender=room.user_chat_id, recipient=room.operator_id,
            chat_id=room.user_chat_id, item_id=message.id
        )

        if stack.get('operator_id') and stack['operator_id'] == room.operator_id and \
                stack['user_id'] == room.user_chat_id:
            await self.bot.send_photo(room.operator_id,
                                      photo=open(f'media/instagram/photos/{photo_name}.jpg', 'rb'))
        else:
            await self.create_unread_msg(
                text=f'{photo_name}.jpg photo instagram', sender=room.user_chat_id, item_id=message.id
            )

    async def create_text_message(self, message, room):
        await self.create_msg(
            text=message.text, sender=room.user_chat_id, recipient=room.operator_id,
            chat_id=room.user_chat_id, item_id=message.id
        )

        if stack.get('operator_id') and stack['operator_id'] == room.operator_id and \
                stack['user_id'] == room.user_chat_id:
            await self.bot.send_message(room.operator_id, message.text)
        else:
            await self.create_unread_msg(
                text=message.text, sender=room.user_chat_id, item_id=message.id
            )

    @staticmethod
    async def filter_messages_from_db(room):
        messages = await Message.query.where(Message.room_id == room.id).gino.all()
        messages_id = []
        for message in messages:
            if message.item_id != 'None':
                messages_id.append(int(message.item_id))

        return messages_id
