import os
import sys
import time

from aiogram import Bot, Dispatcher, types
from aiogram.contrib.fsm_storage.memory import MemoryStorage
from webwhatsapi import WhatsAPIDriver

from data import config
from data.config import instagram_username, instagram_password

bot = Bot(token=config.BOT_TOKEN, parse_mode=types.ParseMode.HTML)
storage = MemoryStorage()
dp = Dispatcher(bot, storage=storage)

# os.environ['SELENIUM'] = 'http://localhost:4444/wd/hub'
print("Environment", os.environ)
try:
    os.environ["SELENIUM"]
except KeyError:
    print("Please set the environment variable SELENIUM to Selenium URL")
    sys.exit(1)

profiledir = os.path.join(".", "firefox_cache")
if not os.path.exists(profiledir):
    os.makedirs(profiledir)

time.sleep(10)
driver = WhatsAPIDriver(
    profile=profiledir, client="remote", command_executor=os.environ["SELENIUM"]
)

# from instagrapi import Client

# instagram = Client()
# instagram.login(username=instagram_username, password=instagram_password)
