import asyncio
import datetime
import os
import subprocess
import sys

import requests
from PIL import Image
from aiogram import executor

from data import database
from data.config import ADMINS, operators, delta, instagram_username
from data.database import create_db, load_operators, load_admins
from data.models import UnReadMessage, Message, Room
import middlewares
import filters
import handlers
from instagram import InstagramBroadCast
from loader import dp, driver
from utils.notify_admins import on_startup_notify
from whatsapp import WhatsAppBroadCast

db = database.DBCommands()


async def on_startup(dispatcher):
    await on_startup_notify(dispatcher)
    await create_db()
    await load_operators()
    await load_admins()


async def periodic(sleep_for):
    await asyncio.sleep(10)
    driver.get_qr(filename='qr.png')
    file = open('qr.png', 'rb')
    await dp.bot.send_photo(ADMINS[0], photo=file)
    os.remove('qr.png')
    await dp.bot.send_message(ADMINS[0], 'Waiting for QR')
    driver.wait_for_login()
    await dp.bot.send_message(ADMINS[0], 'Saving session')
    driver.save_firefox_profile(remove_old=False)

    while True:
        await asyncio.sleep(sleep_for)
        await WhatsAppBroadCast().create(driver=driver, bot=dp.bot)
        # await InstagramBroadCast().create(driver=instagram, bot=dp.bot)


if __name__ == '__main__':
    loop = asyncio.get_event_loop()
    loop.create_task(periodic(1))

    executor.start_polling(dp, on_startup=on_startup, loop=loop)
    # executor.start_polling(dp, on_startup=on_startup)
